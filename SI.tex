
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% This is a (brief) model paper using the achemso class
%% The document class accepts keyval options, which should include
%% the target journal and optionally the manuscript type.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\documentclass[journal=jacsat,manuscript=article]{achemso}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Place any additional packages needed here.  Only include packages
%% which are essential, to avoid problems later.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage{chemformula} % Formula subscripts using \ch{}
\usepackage[version=3]{mhchem}%chemical symbols
\usepackage{tabularx}
\usepackage[T1]{fontenc} % Use modern font encodings
\usepackage{xr}
\externaldocument{Mg2Sb2_annealing}
\renewcommand{\thefigure}{S\arabic{figure}} %put subsection number on fig. numbering
\renewcommand{\thesection}{S\arabic{section}} %put subsection number on fig. numbering
\SectionNumbersOn
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% If issues arise when submitting your manuscript, you may want to
%% un-comment the next line.  This provides information on the
%% version of every file you have used.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%\listfiles

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Place any additional macros here.  Please use \newcommand* where
%% possible, and avoid layout-changing macros (which are not used
%% when typesetting).
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newcommand*\mycommand[1]{\texttt{\emph{#1}}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Meta-data block
%% ---------------
%% Each author should be given as a separate \author command.
%%
%% Corresponding authors should have an e-mail given after the author
%% name as an \email command. Phone and fax numbers can be given
%% using \phone and \fax, respectively; this information is optional.
%%
%% The affiliation of authors is given after the authors; each
%% \affiliation command applies to all preceding authors not already
%% assigned an affiliation.
%%
%% The affiliation takes an option argument for the short name.  This
%% will typically be something like "University of Somewhere".
%%
%% The \altaffiliation macro should be used for new address, etc.f
%% On the other hand, \alsoaffiliation is used on a per author basis
%% when authors are associated with multiple institutions.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\author{Max Wood}
\affiliation{Northwestern University, Evanston, IL 60208, USA}

\author{Jimmy Jiahong Kuo}
\affiliation{Northwestern University, Evanston, IL 60208, USA}

\author{Kazuki Imasato}
\affiliation{Northwestern University, Evanston, IL 60208, USA}

\author{G. Jeffrey Snyder}
\email{jeff.snyder@northwestern.edu}
\affiliation{Northwestern University, Evanston, IL 60208, USA}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% The document title should be given as usual. Some journals require
%% a running title from the author: this should be supplied as an
%% optional argument to \title.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\title[]{Supporting Information: Improvement of Low-Temperature $zT$ in a \ce{Mg3Sb2}-\ce{Mg3Bi2} Solid Solution via Mg-Vapor Annealing}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Some journals require a list of abbreviations or keywords to be
%% supplied. These should be set up here, and will be printed after
%% the title and author information, if needed.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\keywords{thermoelectrics, grain boundary, Mg-vapor anneal, \ce{Mg3Sb2}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% The manuscript does not need to include \maketitle, which is
%% executed automatically.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% The "tocentry" environment can be used to create an entry for the
%% graphical table of contents. It is given here as some journals
%% require that it is printed as part of the abstract page. It will
%% be automatically moved as appropriate.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\begin{tocentry}

%\end{tocentry}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% The abstract environment will automatically gobble the contents
%% if an abstract is not used by the target journal.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Effect of short-term anneal on the n-type properties}\label{short-term}
Fig.~\ref{fig:SeebeckChangeType} shows the effect of short-term Mg-vapor anneal on the Seebeck coefficient. The Te-doped \ce{Mg3Sb2} sample was made nominally slightly Sb-excess/Mg-poor (\textit{i.e.} \ce{Mg_{3-$\delta$}Sb2}) and therefore was p-type initially\cite{Ohno2018}. The sample was annealed in Mg vapor for 1 hour and became degenerately doped n-type afterward. This result demonstrates that a Mg-vapor anneal provides an environment that reduces the Mg vacancies concentration and favors the n-type properties of \ce{Mg3Sb2}-based compounds.


\begin{figure}[bth]
	\includegraphics[width=0.45\textwidth]{./Figures/FIG_SeebeckChangeType_ver02.pdf} 
	\caption{Transition of Te-doped \ce{Mg3Sb2} from the p-type, non-degenerate (blue curve) to n-type, degenerate (red curve) semiconductor after Mg-vapor anneal.}
	\label{fig:SeebeckChangeType}
\end{figure}

\section{Test of sample homogeneity after annealing}
\begin{figure}[bth]
	\includegraphics[width=0.45\textwidth]{./Figures/FIG_PolishTest_muH_ver02.pdf} 
	\caption{Hall mobility of \ce{Mg_{3+$\delta$}Sb_{1.5}Bi_{0.5}} before and after 24-hour anneal. Repeatable n-type Hall mobility can be measured after polishing off 0.5~mm and 1.0~mm (\textit{i.e.} originally 1.7~mm), suggesting that the change is not a surface effect.}
	\label{fig:SurfaceEffectTest}
\end{figure}
All the transport data are interpreted with the assumption that samples are homogeneous without surface effects after the anneal. This is verified via repeated measurements after polishing off the outer surfaces. Fig.~\ref{fig:SurfaceEffectTest} shows that Hall mobility is unaltered after thinning the annealed pellet by 1.0~mm (more than half of the original pellet). 

\section{Pseudo-eutectic temperature measurement}\label{eutectic}
The pseudo-eutectic onset of melting temperature between Mg \& \ce{Mg3Sb_{1.5}Bi_{0.5}} was measured through differential thermal analysis (DTA). Fig.\ref{fig:DTA_data} shows the onset of melting in ball milled powder with composition \ce{Mg_{0.75}(Sb_{0.75}Bi_{0.25})_{0.25}}. 

\begin{figure}[bth]
	\includegraphics[width=0.5\textwidth]{./Figures/FIG_Heating_DTA.pdf} 
	\caption{Differential Thermal Analysis data for ball milled powder of \ce{Mg_{0.75}(Sb_{0.75}Bi_{0.25})_{0.25}} during heating. }
	\label{fig:DTA_data}
\end{figure}

\section{Electrical transport properties measured from ZEM}\label{ZEM3}
For the purpose of comparing $zT$ of similar compounds from the literature, we measure the electrical conductivity and Seebeck coefficient of our annealed 800$^{\circ}$C-sintered sample on an ULVAC ZEM3, as shown in Fig.~\ref{fig:ZEM_data}.
\begin{figure}[bth]
	\includegraphics[width=0.5\textwidth]{./Figures/FIG_Conductivity&Seebeck_ZEM_ver02.pdf} 
	\caption{Electrical conductivity (cyan, left axis) and Seebeck coefficient (purple, right axis) of the annealed 800$^{\circ}$C-sintered sample. The conductivity from ZEM-3 (cyan triangles) shows similar trend as our measured through Van der Pauw measurement (cyan circles), whereas the Seebeck coefficient from ZEM-3 (purple triangles) is larger than the measurement setup designed by Iwanaga et al.\cite{Iwanaga2011} at high temperatures, which may be due to the cold-finger effect\cite{Borup2015}.}
	\label{fig:ZEM_data}
\end{figure}

\section{Hall Mobility}\label{HallMobility}
Fig.~\ref{fig:muH} shows experimental measurement of Hall mobility. The annealed 800$^{\circ}$~C-sintered sample has the highest reported Hall mobility ($\approx 170 \mathrm{cm^2/Vs}$) at 300~K among all n-type \ce{Mg_{3+$\delta$}Sb_{1.5}Bi_{0.5}} compounds (see Table.~\ref{table:muHComparison}).
\begin{figure}[bth]
	\includegraphics[width=0.45\textwidth]{./Figures/FIG_HallMobility_ourData.pdf} 
	\caption{Hall mobility of the annealed (red color) and control (blue color) \ce{Mg_{3+$\delta$}Sb_{1.5}Bi_{0.5}Te_{0.01}} Both the 800$^{\circ}$C-sintered (empty circle marker) and the 600$^{\circ}$C-sintered samples show an improved conductivity after 65-hour Mg-vapor anneal.}
	\label{fig:muH}
\end{figure}

\begin{table}[bth]
	\caption{\label{table:muHComparison} Hall mobility $\mu_H$ at 300~K of the annealed 800$^{\circ}$C-sintered sample and data from the literature.}
	\begin{tabular}{rcccc}
		\hline\hline
		Ref.&Kanno\cite{Kanno2018}&Zhang\cite{Zhang2017}&Ohno\cite{Ohno2018}\&Shuai\cite{Shuai2017}&This work\\
		%\cline{3-4}
		\hline
		$\mu_H\mathrm{(cm^2/Vs)}$&109&45&$\approx$20&\color{red}170\color{black}\\
		\hline\hline
	\end{tabular}
\end{table}

\section{X-Ray Diffraction Measurement}
X-Ray diffraction on the pelletized was carried out on a Stoe STADI-MP in reflection geometry using pure Cu $\mathrm{K1\alpha}$ radation. From XRD There appears to be no detectable impurity phase in any of the samples tested for this study. The narrowing of peaks in the annealed samples is likely due to micro-strain being reduced in the annealed samples.

\begin{figure}[bth]
	\includegraphics[width=0.45\textwidth]{./Figures/FIG_XRAY.pdf} 
	\caption{Cu $\mathrm{K1\alpha}$ x-ray diffraction in reflection geometry of pelletized  samples sythesized for this study. All samples had a composition of  \ce{Mg_{3.01}Sb_{1.49}Bi_{0.5}Te_{0.01}}, but underwent different processing steps.}
	\label{fig:Xray}
\end{figure}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% The appropriate \bibliography command should be placed here.
%% Notice that the class file automatically sets \bibliographystyle
%% and also names the section correctly.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\bibliography{Mg3Sb2_annealing.bib}

\end{document}
