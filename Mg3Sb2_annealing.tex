
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% This is a (brief) model paper using the achemso class
%% The document class accepts keyval options, which should include
%% the target journal and optionally the manuscript type.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\documentclass[journal=jacsat,manuscript=article]{achemso}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Place any additional packages needed here.  Only include packages
%% which are essential, to avoid problems later.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage{chemformula} % Formula subscripts using \ch{}
\usepackage[version=3]{mhchem}%chemical symbols
\usepackage{tabularx}
\usepackage[T1]{fontenc} % Use modern font encodings
\usepackage{xr}
\usepackage{multirow}
\externaldocument{SI}
\SectionNumbersOn
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% If issues arise when submitting your manuscript, you may want to
%% un-comment the next line.  This provides information on the
%% version of every file you have used.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%\listfiles

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Place any additional macros here.  Please use \newcommand* where
%% possible, and avoid layout-changing macros (which are not used
%% when typesetting).
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newcommand*\mycommand[1]{\texttt{\emph{#1}}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Meta-data block
%% ---------------
%% Each author should be given as a separate \author command.
%%
%% Corresponding authors should have an e-mail given after the author
%% name as an \email command. Phone and fax numbers can be given
%% using \phone and \fax, respectively; this information is optional.
%%
%% The affiliation of authors is given after the authors; each
%% \affiliation command applies to all preceding authors not already
%% assigned an affiliation.
%%
%% The affiliation takes an option argument for the short name.  This
%% will typically be something like "University of Somewhere".
%%
%% The \altaffiliation macro should be used for new address, etc.f
%% On the other hand, \alsoaffiliation is used on a per author basis
%% when authors are associated with multiple institutions.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\author{Max Wood}
\affiliation{Northwestern University, Evanston, IL 60208, USA}

\author{Jimmy Jiahong Kuo}
\affiliation{Northwestern University, Evanston, IL 60208, USA}

\author{Kazuki Imasato}
\affiliation{Northwestern University, Evanston, IL 60208, USA}

\author{G. Jeffrey Snyder}
\email{jeff.snyder@northwestern.edu}
\affiliation{Northwestern University, Evanston, IL 60208, USA}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% The document title should be given as usual. Some journals require
%% a running title from the author: this should be supplied as an
%% optional argument to \title.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\title[]{Improvement of Low-Temperature $zT$ in a \ce{Mg3Sb2}-\ce{Mg3Bi2} Solid Solution via Mg-Vapor Annealing}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Some journals require a list of abbreviations or keywords to be
%% supplied. These should be set up here, and will be printed after
%% the title and author information, if needed.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\keywords{thermoelectrics, grain boundary, vapor anneal, \ce{Mg3Sb2}, ionized impurity, scattering, coarsening}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% The manuscript does not need to include \maketitle, which is
%% executed automatically.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% The "tocentry" environment can be used to create an entry for the
%% graphical table of contents. It is given here as some journals
%% require that it is printed as part of the abstract page. It will
%% be automatically moved as appropriate.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\begin{tocentry}

%\end{tocentry}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% The abstract environment will automatically gobble the contents
%% if an abstract is not used by the target journal.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{abstract}
	Materials with high $zT$ over a wide temperature range are essential for thermoelectric applications. N-type \ce{Mg3Sb2}-based compounds have been shown to achieve high $zT$ at  700~K, but their performance at low temperatures ($<$500~K) is compromised due to their highly-resistive grain boundaries. Syntheses and optimization processes to mitigate this grain boundary effect has been limited due to the loss of Mg which hinders a sample's n-type dopability. In this work, we demonstrate that a Mg-vapor anneal processing step that grows a sample's grain size and preserves its n-type carrier concentration during annealing. The electrical conductivity and mobility of the samples with large grain size follows a phonon-scattering-dominated $T^{-3/2}$ trend over a large temperature range further supporting the conclusion that the temperature-activated mobility in \ce{Mg3Sb2}-based materials is caused by resistive grain boundaries. The measured Hall mobility of electrons reaches 170 $\mathrm{cm^2}$/Vs in annealed 800$^{\circ}$C-sintered \ce{Mg_{3+\delta}Sb_{1.49}Bi_{0.5}Te_{0.01}}, the highest ever reported for \ce{Mg3Sb2}-based thermoelectric materials. In particular, this sample with grain size $> 30\ \mu m$ has a $zT$ 0.8 at 300~K, which is comparable to comercial thermoelectric materials used at room temperature (n-type \ce{Bi2Te3}) while reaching $zT$ 1.4 at 700~K, allowing  applications over a wider temperature scale.
\end{abstract}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Start the main part of the manuscript here.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction}
%% Paragraph purpose: Argue low temp zT matters
Thermoelectric materials have the ability to interconvert gradients in temperature with electric potential\cite{Snyder2011}. These materials can be used as solid-state engines directly converting heat into electricity, or Peltier coolers allowing for efficient scalable refrigeration. The maximum energy conversion efficiency of a thermoelectric material is quantified by a temperature-dependent thermoelectric figure of merit $zT$. Although the community often focuses on peak $zT$ values at one temperature, it is more important for a device to have good performance throughout the wide temperature difference, which is characterized by the device $ZT$\cite{Snyder2017}. 

\begin{figure}[bth]
	\includegraphics[width=0.45\textwidth]{./Figures/FIG_AnnealSchematic.pdf} 
	\caption{Schematic of the Mg-vapor annealing system. The sample pellet and Mg turnings are put in a MgO crucible. The crucible is loaded into a graphite susceptor, which is heated up to the target temperature (\textit{e.g.} 600$^{\circ}$C) via an induction heater.}
	\label{fig:Schematic}
\end{figure}

%% Paragraph purpose: Introduce activated mobility in Mg3Sb2
Recently, n-type \ce{Mg3Sb2}-based compounds were discovered with a promising peak $zT\approx1.6$ at 700~K spurring great interests in related compounds\cite{Tamaki2016, Shuai2017, Zhang2017}. However, low \textit{zT} due to the grain boundary scattering at low temperature (300~K to 500~K) not only makes the material uncompetitive at low temperature but also reduces its performance at high temperatures \cite{Kuo2018}. Based on our previous study\cite{Kuo2018}, we estimated room for $>$60\% improvement of $zT$ at room temperature if the effects of grain boundaries were entirely eliminated. While hot-pressing at elevated temperatures (\textit{e.g.} 800$^{\circ}$C) has been demonstrated to increase the average grain size\cite{Kanno2018} and therefore $zT$, further grain growth through long-term heat treatment has been limited as the loss of Mg prohibits the n-type dopability of the compounds\cite{Ohno2018}. 


In this work, further grain growth is achieved through a novel annealing procedure in the presence of Mg-vapor (Fig.~\ref{fig:Schematic}) which preserves the heavily doped n-type properties of the materials. Mg-vapor annealing can significantly improve the performance of a sample at low temperature by increasing its average grain size. In particular, a 800$^{\circ}$C-sintered Te-doped \ce{Mg_{3+\delta}Sb_{1.5}Bi_{0.5}} reaches a $zT\approx0.8$ at 300~K after long-term annealing (\textit{e.g.} $\approx$ 65 hours, see Fig.~\ref{fig:zT}(a)) which is comparable to commercial thermoelectric material used at room temperature (n-type Se-alloyed \ce{Bi2Te3}). This high $zT$ is due to the sample's high weighted mobility (Fig.~\ref{fig:zT}(b)), which characterizes its potential power factor\cite{May2017}. In addition, the Hall mobility reaches 170 $\mathrm{cm^2/Vs}$ (see section.~\ref{HallMobility} in the SI), which is the highest among all reported data for n-type \ce{Mg_{3}Sb_{1.5}Bi_{0.5}}.

\begin{figure*}[t!]
	\includegraphics[width=0.9\textwidth]{./Figures/FIG_zT&WeightedMobility_compairison_ver02.pdf}
	\caption{(a) Temperature dependent $zT$ and (b) Weighted mobility\cite{May2017} (calculated using electrical conductivity and Seebeck coefficient) of \ce{Mg_{3+\delta}Sb_{1.49}Bi_{0.5}Te_{0.01}} annealed for 65 hours in Mg vapor in comparison to similar compositions found in literature\cite{Zhang2017,Shuai2017, Kanno2018, Ohno2018} and n-type \ce{Bi_{2}Te_{3}}\cite{Marlow}. Note that the electrical conductivity and Seebeck coefficients used here are all measured in a ZEM-3 for the purpose of comparison (see section.~\ref{ZEM3}).}
	\label{fig:zT}
\end{figure*}
%\FloatBarrier



\section{Magnesium-vapor Anneal}
Annealing has long been a strategy for improving the electrical mobility in materials, which can be due to several different mechanisms: reduction of defects (vacancy, interstitial, dislocation), grain growth, and grain boundary modification. etc.\cite{Warzecha2012,schultz1962effects,bardeen1940electrical,sekimoto2005annealing,Watanabe2011}. Conventional annealing of thermoelectric materials is typically done in a vacuum sealed fused-quartz container to prevent oxidation. For n-type \ce{Mg3Sb2}, however, this method is not applicable due to the reaction between Mg and quartz to form Magnesium silicates or MgO. Even the use of an open crucible (\textit{e.g.} metal foil or vitreous carbon – \ce{Al2O3} will also react with Mg) will not suffice because of the significant vapor pressure of Mg which will quickly react with \ce{SiO2}. The loss of Mg promotes the formation of Mg vacancies $\mathrm{V^{2-}_{Mg}}$, which serves as electron killers and effectively eliminates the n-type charge carriers in the samples\cite{Ohno2018}.  

In this work, we develop an unconventional technique (Fig.~\ref{fig:Schematic}) which preserves the n-type properties of \ce{Mg3Sb2}-based compounds throughout the annealing precess. In this technique , hot-pressed pellets are placed into a MgO crucible together with Mg turnings (elemental Mg). MgO was choosen for the crucible because it has no sub-oxides and therefore wouldn't react with the sample, Mg vapor or Mg metal. The crucible was then loaded into a graphite susceptor which was heated up to the target temperature via an induction heater. Due to the fact Mg forms no stable carbides, a Mg vapor pressure can be maintained inside the crucible and in equilibrium with the annealing sample. 

The Mg vapor present as a secondary phase during the Mg-vapor anneal, is necessary to maintain high n-type carrier concentration in \ce{Mg3Sb2}-based materials. Without the presence of Mg vapor during a 600$^{\circ}$C anneal, degenerate n-type samples become non-degenerate p-type. A similar effect has been observed in our previous work\cite{Imasato2018}, in which we showed the depletion of n-type charge carriers in Te-doped \ce{Mg_{3+$\delta$}Sb_{1.5}Bi_{0.5}} over time when the sample was held under dynamic vacuum at 450$^{\circ}$C. The effect was attributed to the preferential sublimation of Mg, which lead to the creation of electron neutralizing Mg vacancy point defects.

When these intrinsic samples are then annealed in the presence of Mg vapor, their degenerate n-type charge carrier concentration returns, confirming that preferential sublimation of Mg is the cause of the aforementioned reduction of carrier concentration. Additionally, Te-doped samples that are intentionally made Mg deficient and non-degenerate (low charge carrier concentration) can even be put into a Mg-excess degenerate state via a Mg vapor anneal (\textit{e.g.} 1 hour at 600$^{\circ}$C, see section.~\ref{short-term} in the SI). This shows excess Mg in a sample's nominal composition is not necessary to achieve n-type conduction as long as Mg vacancies are controlled via some other method.

\begin{figure*}[t]
	\includegraphics[width=0.9\textwidth]{./Figures/FIG_BiTrans&EBSDMaps_ver03.pdf} 
	\caption{(a)-(c) Transport properties of the annealed (solid markers) and control (open markers) \ce{Mg_{3+$\delta$}Sb_{1.49}Bi_{0.5}Te_{0.01}} Both the 800$^{\circ}$C-sintered (red-circles) and the 600$^{\circ}$C-sintered samples(blue-triangles) show an improved conductivity after 65-hour Mg-vapor anneal. The annealed 800$^{\circ}$C-sintered sample (red closed circle) shows a $T^{-3/2}$-trend as predicted by phonon (deformation potential) scattering. The negligible change of Seebeck coefficient and thermal conductivity is consistent with our previous study assuming a grain boundary scattering\cite{Kuo2018}. (d)-(g) Corresponding EBSD maps of the samples. Grain growth observed in the annealed samples suggests the improved conductivity in (a) is due to the reduction of grain-boundary density.}
	{\tiny {\tiny }}	\label{fig:BiTrans}
\end{figure*}


\section{Experimental Result}
\subsection{Transport Properties}
%%Introduce the affects of annealing on the carrier concentration/fermi level
A significant enhancement of electrical conductivity at low temperatures was discovered in the \ce{Mg_{3+$\delta$}Sb_{1.49}Bi_{0.5}Te_{0.01}} samples after long-term Mg-vapor anneal (Fig.~\ref{fig:BiTrans}(a)). Pellets with different initial grain sizes were prepared via different sintering conditions (\textit{i.e.} at 600$^{\circ}$C for 1 hour and at 800$^{\circ}$C for 20 minutes). In each condition, a pair of pellets were pressed at the same time in the same instrument, with one as the control sample and the other for annealing at 600$^{\circ}$C for 65 hours. For both conditions, annealed samples show higher electrical conductivities compared to the un-annealed control ones. 

In particular, the conductivity of the 800$^{\circ}$C-sintered sample follows a $T^{-3/2}$-trend, which indicates a phonon (deformation potential) scattering mechanism dominating even at room temperature. The unchanged Seebeck (Fig.~\ref{fig:BiTrans}(b)) suggests that the Fermi level of the sample is unaltered, and therefore the improvement of conductivity should be attributed to the reduction of detrimental grain boundary scattering. Due to the phonon (deformation potential) scattering temperature dependence of mobility (Fig.~\ref{fig:zT}(b)), additional annealing would likely not continue to improve the electrical mobility of our 800$^{\circ}$C-sintered and annealed sample.

\subsection{Grain Size Measurement}
Electronically, the region near a grain boundary can be more resistive than the bulk due to the disorder and off-stoichiometry at the boundary\cite{Taylor1952,Pike1979}. Decreasing the density of grain boundaries, as well as reducing their resistance are two potential explanations for the improvement of low temperature electrical conductivity shown in Fig.~\ref{fig:BiTrans}(a). Experimentally, the average grain sizes before and after anneal can be measured via electron backscatter diffraction (EBSD). 

Measurable grain growth in the annealed samples can be seen through the comparison of EBSD maps (Fig.~\ref{fig:BiTrans}(d)-(h)). The grain sizes of the un-annealed samples (\textit{i.e.} $<1\ \mu m$ for the 600$^{\circ}$C-sintered, and $\approx10\ \mu m$ for the 800$^{\circ}$C sintered) is consistent with the values reported in the literature\cite{Kanno2018}, whereas the annealed samples have grain size significantly larger than the un-annealed counterparts (\textit{i.e.} $\approx5\ \mu m$ for the 600$^{\circ}$C-sintered, and $>30\ \mu m$ for the 800$^{\circ}$C sintered). 


\section{Discussion}
\subsection{Implication of charge transport}
The annealed samples' enhanced electrical conductivity and unchanged Seebeck coefficient after grain growth supports the conclusion that grain boundary scattering is significant in \ce{Mg3Sb2}-based materials \cite{Kuo2018}. Because a sample's measured Seebeck coefficient is dominated by its bulk electrical properties (\textit{e.g.} density-of-states and Fermi level)\cite{May2017}, when the transport mechanism of the bulk grains changes, the Seebeck coefficient should also change correspondingly. Based on this argument, other hypotheses for why the electrical conductivity increses with temperature in some n-type \ce{Mg3Sb2}, such as point-defect scattering or ionized-impurity scattering\cite{Shuai2017,Mao2017}, would predict a change of bulk properties and therefore a change of Seebeck coefficient. In addition, these hypotheses do not explain the enhanced performance witnessed in samples with large grain sizes\cite{Kanno2018}. We therefore conclude that the reduction in low-temperature $zT$ observed must be due to the highly-resistive grain boundaries.

\begin{table}[bth]
	\caption{\label{table:melting} Eutectic melting temperatures between Mg - \ce{Mg3Sb2}\cite{Nayeb-Hashemi1984,grube1906alloys,abel1930ternare,grube1934elektrische}  and Mg - \ce{Mg3Bi2}\cite{Nayeb-Hashemi1985,grube1906alloys,abel1930ternare}, and the pseudo-eutectic melting temperature between Mg - \ce{Mg3Sb_{1.5}Bi_{0.5}}. The pseudo-eutectic initial melting temperature  of the alloyed compound was measured through differential thermal analysis in this work (see section.~\ref{eutectic} in the SI).}
	\begin{tabular}{rccc}
		\hline\hline
		{\ce{Mg3(Sb_{1-x}Bi_{x})_{2}}}&x=0&0.25&1\\
		%\cline{3-4}
		\hline
		$T\mathrm{_{Eut} (^{\circ}C)}$&$629 \pm 2$&$602 \pm 5$&$552 \pm 2$\\
		\hline\hline
	\end{tabular}
\end{table}

\subsection{Hypothesis of the Grain-growth Mechanism}
%%Paragraph on Grain growth
The grain growth witnessed in annealed samples could be associated with a pseudo-eutectic liquid that forms at the grain boundary, and dramatically increases atomic diffusion during annealing.\cite{german2014sintering}  This liquid is called pseudo-eutectic (like in the Al - \ce{Mg2Si} pseudo-binary \cite{FanZ2001EpA2,LiChong2009Mfih}) instead of eutectic because it does not meet the strict criteria of being a single point in ternary space where four phases are in equilibrium at a single temperature.\cite{campbell2012phase, Callister2007} Furthermore, the term pseudo-eutectic is used to differentiate the liquid that we witness from a congruently melted liquid formed entirely from the \ce{Mg_{3}Sb_{1.5}Bi_{0.5}} alloy, which would only occur at a much higher temperature. 

The pseudo-eutectic temperature is assumed to be the temperature of initial melting  measured by DTA as, and changes with Sb/Bi ratio as shown in Table ~\ref{table:melting}. The Mg - \ce{Mg_{3}Sb_{2}} eutectic has been reported at 88 at.\% \cite{grube1934elektrische} and 90 at.\% \cite{grube1906alloys} of Mg. The Mg - \ce{Mg_{3}Bi_{2}} eutectic has been reported at 87 at.\% \cite{Nayeb-Hashemi1985}, 82 at.\%\cite{grube1906alloys} and 86 at.\%\cite{grube1934elektrische} of Mg. Therefore, we estimate the pseudo-eutectic composition should be found at approximately  at $85 \pm5 $ at.\% of Mg. The Te-doped $\mathrm{Mg_{3+\delta} Sb_{1.5}Bi_{0.5}}$ samples presented here are annealed with extra Mg at a temperature where there is likely to be some liquid phase, which would increase grain growth.\cite{german2014sintering}   A quick non-equilibrium re-solidification of this melt should lead to some compositional segregation of the Sb/Bi ratios in solid-solution, which is called coring \cite{campbell2012phase, Callister2007}. 


\section{Conclusions}
In conclusion, we demonstrate an unconventional method for annealing n-type \ce{Mg3Sb2}-based compounds in Mg vapor which preserves their n-type properties during grain growth. We observed that long-term annealing results in a significant enhancement in the electron mobility, which is attributed to a reduction in grain boundary scattering as the grain size increased. Adding this annealing processing step to \ce{Mg_{3+$\delta$}Sb_{1.49}Bi_{0.5}Te_{0.01}} material results in a $zT$ = 0.8 at 300~K allowing it to compete with commercial n-type thermoelectric materials. 


\footnotesize

\section{Experimental Methods}
\subsection{Sample preparation}
We sealed magnesium turnings (99.98 \%, Alfa Aesar), antimony shots (99.9999 \%, 5N Plus), and Te shot (99.999 \%, 5N Plus) into stainless-steel vials according to stoichemetric ratios in an argon-filled glove box.The nominal composition we used for all ball milled samples in this study was \ce{Mg_{3.01}Sb_{1.49}Bi_{0.5}Te_{0.01}}.  Elements were mechanically alloyed by high energy-ball milling with a high-energy mill (SPEX 8000D) for two hours. The processed powder was loaded into a graphite die with  half inch diameter and pressed by an induction heating rapid hot press \cite{Lalonde2011} for 60 minutes at 873~K or 20 minutes at 1073~K and 45 MPa under argon gas flow. 

\subsection{Annealing process}
Hot-pressed pellets were placed into a magnesium oxide crucible (25~mm diameter, 25~mm height). Magnesium turnings (99.98 \%, Alfa Aesar) are added such that both sides of the pellet are in contact with Mg turnings. The crucible was loaded into a graphite die and covered by a piece of graphite foil and graphite spacer to create a quasi-isolating environment for the pellet. The graphite die was heated up by a induction heater to 873~K under argon gas flow. 

\subsection{Measurement of crystal grain sizes}
Electron backscattering diffraction (EBSD) maps were obtained using a scanning electron microscope (Quanta650FEG) equipped with a detector (Oxford Instruments Nordlys).

\subsection{Differential thermal analysis}
Differential Thermal Analysis (DTA) was carried out in a Netzch Jupiter F3. A balled milled samples of \ce{Mg_{0.75}(Sb_{0.75}Bi_{0.25})_{.25}} was sealed in a carbon coated quartz vials and ramped to 750 $^{\circ}$C at a rate of 10 $^{\circ}$C/min. The sample was then cooled at a rate of 10 $^{\circ}$ to room temperature. 

\subsection{Measurements of transport properties}
Electrical and thermal transport properties were measured from 323 to 573~K. The electrical resistivity and Hall coefficient measurements were determined using the 4-point probe Van der Pauw technique with a 0.8~T magnetic field under high vacuum \cite{Borup2015}. The Seebeck coefficients of the samples were obtained using chromel-Nb thermocouples by applying a temperature gradient across the sample to oscillate between $\pm$5~K \cite{Iwanaga2011}. Thermal conductivity was calculated from the relation $\kappa = DdC_p$, where $D$ is the thermal diffusivity measured with a Netzsch LFA 457 laser flash apparatus, $d$ is the geometrical density of the material (see Table.~\ref{table:density}) and $C_p$ is the heat capacity at constant pressure. $C_p$ the compounds were calculated via the polynomial equation proposed by Agne \textit{et al.}\cite{Agne2018}. For the purpose of comparison, we measured the electrical transport properties (\textit{i.e.} conductivity and Seebeck coefficient) on an ULVAC ZEM-3, as shown in Fig.~\ref{fig:ZEM_data} in the SI. ZEM data is used for to compare $zT$'s and weighted mobilities of our samples and literature (Fig.~\ref{fig:zT}).

\begin{table}[bth]
	\caption{\label{table:density} Geometrical density $d$ of the samples sintered at different temperatures with and without annealing. Theoretical estimate of the density of \ce{Mg3Sb_{1.5}Bi_{0.5}} is calculated from densities of the two end members assuming a linear relation(\textit{i.e.} 4.02 $\mathrm{g/cm^3}$ for \ce{Mg3Sb2} and 5.84 $\mathrm{g/cm^3}$ for \ce{Mg3Bi2}, data from the Inorganic Crystal Structure Database).}
	\begin{tabular}{cccccc}
		\hline\hline
		$T_\mathrm{sinter}$ & \multicolumn{2}{c}{600$^{\circ}$C} & \multicolumn{2}{c}{800$^{\circ}$C} & Theo.\\ 
		\hline
		\multirow{2}{*}{d ($\mathrm{g/cm^3}$)} &pristine &anneal &pristine &anneal &\multirow{2}{*}{$\approx$4.5} \\
		& 4.51 & 4.45 & 4.49 & 4.48\\ 
		
		\hline\hline
	\end{tabular}
\end{table}

\normalsize
\section{Author contributions}
MW and JJK equally contributed to this project. MW designed the annealing setup. MW and JJK conceived the project and wrote the manuscript.  MW, JJK, and KI synthesized and processed the compounds, performed characterizations, and measured/analyzed transport data. GJS supervised the project. All authors edited the manuscript.





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% The "Acknowledgement" section can be given in all manuscript
%% classes.  This should be given within the "acknowledgement"
%% environment, which will make the correct section or running title.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{acknowledgement}
This research was carried out under a contract with the National Aeronautics and Space Administration and was supported by the NASA Science Missions Directorate's Radioisotope Power Systems Technology Advancement Program. The EBSD in this work made use of the EPIC facility of Northwestern University’s NUANCE Center, which has received support from: the Soft and Hybrid Nanotechnology Experimental (SHyNE) Resource (NSF ECCS-1542205); the MRSEC program (NSF DMR-1720139) at the Materials Research Center; the International Institute for Nanotechnology (IIN); the Keck Foundation; the State of Illinois, through the IIN. DTA and XRD in this work made use of the IMSERC at Northwestern University, which has received support from the Soft and Hybrid Nanotechnology Experimental (SHyNE) Resource (NSF ECCS-1542205); the State of Illinois and International Institute for Nanotechnology (IIN).
\end{acknowledgement}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% The appropriate \bibliography command should be placed here.
%% Notice that the class file automatically sets \bibliographystyle
%% and also names the section correctly.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\bibliography{Mg3Sb2_annealing.bib}

\end{document}
