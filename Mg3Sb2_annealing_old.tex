%***** Start of file aipsamp.tex ******
%
%   This file is part of the AIP files in the AIP distribution for REVTeX 4.
%   Version 4.1 of REVTeX, October 2009
%
%   Copyright (c) 2009 American Institute of Physics.
%
%   See the AIP README file for restrictions and more information.
%
% TeX'ing this file requires that you have AMS-LaTeX 2.0 installed
% as well as the rest of the prerequisites for REVTeX 4.1
%
% It also requires running BibTeX. The commands are as follows:
%
%  1)  latex  aipsamp
%  2)  bibtex aipsamp
%  3)  latex  aipsamp
%  4)  latex  aipsamp
%
% Use this file as a source of example code for your aip document.
% Use the file aiptemplate.tex as a template for your document.
\documentclass[%
aip,
rsi,
amsmath,amssymb,
preprint,%
%reprint,%
]{revtex4-1}

\usepackage{graphicx}% Include figure files
\usepackage{dcolumn}% Align table columns on decimal point
\usepackage{bm}% bold math
\usepackage[version=3]{mhchem}%chemical symbols
%\usepackage[mathlines]{lineno}% Enable numbering of text and display math
%\linenumbers\relax % Commence numbering lines
\usepackage[none]{hyphenat} %suppress hyphenation
\usepackage{multirow}
\usepackage{float} %use [H] to force figure placement
\usepackage{color}
\usepackage{placeins}
\usepackage{gensymb}
%\setlength{\tabcolsep}{20pt}
\renewcommand{\arraystretch}{1.5}
\usepackage{xr}
\externaldocument{SI}

\begin{document}

%\preprint{AIP/123-QED}

\title[]{Improvement of low-temperature $zT$ in \ce{Mg3Sb2}-based compounds via Mg-vapor annealing}% Force line breaks with \\

\author{Max Wood}
\affiliation{Northwestern University, Evanston, IL 60208, USA}

\author{Jimmy Jiahong Kuo}
\affiliation{Northwestern University, Evanston, IL 60208, USA}

\author{Kazuki Imasato}
\affiliation{Northwestern University, Evanston, IL 60208, USA}

\author{G. Jeffrey Snyder}
\email{jeff.snyder@northwestern.edu}
\affiliation{Northwestern University, Evanston, IL 60208, USA}

\date{\today}

\begin{abstract}
Materials with high $zT$ over a wide temperature range are essential for thermoelectric applications. N-type \ce{Mg3Sb2}-based compounds have been shown to achieve high $zT$ at  700~K, but the performance at low temperature ($<$500~K) is compromised due to their highly-resistive grain boundaries. Syntheses and optimization processes to mitigate these grain boundaries has been limited due to the loss of Mg which hinders its n-type dopability. In this work, we demonstrate that a Mg-vapor anneal processing step to grow the grain size and preserves the n-type dopant concentration during annealing. The electrical conductivity and mobility of the samples with large grain size follows a phonon dominated $T^{-3/2}$ trend over a large temperature range further supporting the conclusion that the temperature-activated mobility in \ce{Mg3Sb2}-based materials is caused by resistive grain boundaries. The measured Hall mobility of electrons reaches 170 $\mathrm{cm^2}$/Vs in annealed 800~\degree C \ce{Mg_{3+\delta}Sb_{1.49}Bi_{0.5}Te_{0.01}}, the highest ever reported for \ce{Mg3Sb2}-based thermoelectric materials. In particular, this sample with grain size $> 30\mu m$ has a $zT$ 0.8 at 300K, which is comparable to the state-of-the-art thermoelectric material used at room temperature (n-type Se-alloyed \ce{Bi2Te3}) while reaching $zT$ 1.4 at 700~K, allowing  applications over a wide temperature scale.
\end{abstract}

\keywords{thermoelectrics, grain boundary, Mg-vapor anneal, \ce{Mg3Sb2}}

\maketitle

\section{Introduction}
%% Paragraph purpose: Argue low temp zT matters
Thermoelectric materials have the ability to interconvert gradients in temperature with electric potential\cite{Snyder2011}. These materials can be used as solid-state engines directly converting heat into electricity, or Peltier coolers allowing for efficient scalable refrigeration. The maximum energy conversion efficiency of a thermoelectric material is quantified by a temperature-dependent thermoelectric figure of merit $zT$. Although peak $zT$ values at one temperature are easy to report, it is more important good performance throughout the wide temperature difference between the heat source and the heat sink which is characterized by the device $ZT$. 

%% Paragraph purpose: Introduce activated mobility in Mg3Sb2
Recently, n-type \ce{Mg3Sb2}-based compounds were discovered with a promising peak $zT\approx1.6$ at 700~K spurring great interests in related compounds\cite{Tamaki2016, Shuai2017, Zhang2017}. However, low \textit{zT} due to the grain boundary scattering at low temperature (300~K to 500~K) not only makes the material uncompetitive at low temperature but also reduces its performance at high temperatures \cite{Kuo2018}. Based on our previous study\cite{Kuo2018}, we estimated room for $>$60\% improvement of $zT$ at room temperature if the effects of grain boundary were entirely eliminated. While hot-pressing at elevated temperatures (\textit{e.g.} 800~\degree C) has been demonstrated to increase the average grain size\cite{Kanno2018} and therefore $zT$, further grain growth through long-term heat treatment is limited as the loss of Mg prohibits the n-type dopability of the compounds\cite{Ohno2018}. 

In this work, further grain growth is achieved through a novel annealing procedure in the presence of Mg-vapor (Fig.~\ref{fig:Schematic}) which preserves the heavily doped n-type properties of the materials. Such Mg-vapor annealing (\textit{e.g.} $\approx$65 hours) can significantly improve the performance of a sample at low temperature by increasing the average grain size. In particular, a 800~\degree C-sintered Te-doped \ce{Mg_{3+\delta}Sb_{1.5}Bi_{0.5}} reaches a $zT\approx0.8$ at 300~K after long-term annealing (Fig.~\ref{fig:zT}(a)) which is comparable to the state-of-the-art thermoelectric material used at room temperature (n-type Se-alloyed \ce{Bi2Te3}). Such a high $zT$ is due to the high weighted mobility (Fig.~\ref{fig:zT}(b)), which characterizes the potential power factor\cite{May2017}. In addition, the Hall mobility reach 170 $\mathrm{cm^2/Vs}$ (see section.~\ref{HallMobility}), which is the highest among all reported data for n-type \ce{Mg_{3}Sb_{1.5}Bi_{0.5}}.

\begin{figure*}[t!]
	\includegraphics[width=0.9\textwidth]{./Figures/FIG_zT&WeightedMobility_compairison.pdf}
	\caption{(a) Temperature dependent $zT$ and (b) Weighted mobility\cite{May2017} (calculated using electrical conductivity and Seebeck coefficient) of \ce{Mg_{3+\delta}Sb_{1.49}Bi_{0.5}Te_{0.01}} annealed for 65 hours in Mg vapor in comparison to similar compositions found in literature\cite{Zhang2017,Shuai2017, Kanno2018, Ohno2018} and n-type \ce{Bi_{2}Te_{3-x}Se_{x}}\cite{Hu2015}. Note that the electrical conductivity and Seebeck coefficients used here are all measured in a ZEM-3 for the purpose of comparison (see section.~\ref{ZEM3}).}
	\label{fig:zT}
\end{figure*}
%\FloatBarrier

\begin{figure}[bth]
	\includegraphics[width=0.45\textwidth]{./Figures/FIG_AnnealSchematic.pdf} 
	\caption{Schematic of the Mg-vapor annealing system. The sample pellet and Mg turnings are put in a MgO crucible. The crucible is loaded into a graphite susceptor, which is heated up to the target temperature (\textit{e.g.} 600~\degree C) via an induction heater.}
	\label{fig:Schematic}
\end{figure}

\section{Magnesium-vapor Anneal}
Annealing has long been a strategy for improving the electrical mobility in materials, which can occur due to several different mechanisms, \textit{e.g.} reduction of defects (vacancy , interstitial, dislocation), grain growth, and grain boundary modification. etc.)\cite{Warzecha2012,schultz1962effects,bardeen1940electrical,sekimoto2005annealing,Watanabe2011}. Conventional annealing of thermoelectric materials is typically done in a vacuum sealed fused-quartz container to prevent oxidation. For n-type \ce{Mg3Sb2}, however, this method is not applicable due to the reaction between Mg and quartz to form Magnesium silicates or MgO. Even use of an open crucible (\textit{e.g.} metal foil or vitreous carbon – \ce{Al2O3} will also react with Mg) will not suffice because of the significant vapor pressure of Mg which will quickly react with \ce{SiO2}. The loss of Mg promotes the formation of Mg vacancies $\mathrm{V^{2-}_{Mg}}$, which serves as electron killers and effectively eliminates the n-type charge carriers in the samples\cite{Ohno2018}.  

In this work, we develop an unconventional technique (Fig.~\ref{fig:Schematic}) which preserves the n-type properties of \ce{Mg3Sb2}-based compounds throughout the annealing precess. In this technique , hot-pressed pellets are placed into a MgO crucible together with Mg turnings (elemental Mg). MgO was choosen for the crucible because it has no sub-oxides and therefore wouldn't react with the sample, Mg vapor or Mg metal. The crucible was then loaded into a graphite susceptor which was heated up to the target temperature via an induction heater. Due to the fact Mg forms no stable carbides, a Mg vapor pressure can be maintained inside the crucible and in equilibrium with the annealing sample. 

The Mg vapor present as a secondary phase during the Mg-vapor anneal, is necessary to maintain high n-type carrier concentration in \ce{Mg3Sb2}-based materials. Without the presence of Mg vapor during a 600~\degree C anneal, degenerate n-type samples become non-degenerate p-type. A similar effect has been observed in our previous work\cite{Imasato2018}, in which we showed the depletion of n-type charge carriers in Te-doped \ce{Mg_{3.05}Sb_{1.5}Bi_{0.5}} over time when the sample was held under dynamic vacuum at 450~\degree C. The effect was attributed to the preferential sublimation of Mg, which lead to the creation of electron neutralizing Mg vacancy point defects.

When these intrinsic samples are then annealed in the presence of Mg vapor, their degenerate n-type charge carrier concentration returns, confirming that preferential sublimation of Mg is the cause of the aforementioned reduction of carrier concentration. Additionally, Te-doped samples that are intentionally made Mg deficient and non-degenerate (low charge carrier concentration) can even be put into a Mg-excess degenerate state via a Mg vapor anneal (\textit{e.g.} 1 hour at 600~\degree C, see section.~\ref{short-term})This shows excess Mg in a sample's nominal composition is not necessary to achieve n-type conduction as long as Mg vacancies are controlled via some other method.

\begin{figure*}[t]
	\includegraphics[width=0.9\textwidth]{./Figures/FIG_BiTrans&EBSDMaps_ver03.pdf} 
\caption{(a)-(c) Transport properties of the annealed (red color) and control (blue color) \ce{Mg_{3.01}Sb_{1.5}Bi_{0.5}Te_{0.01}} Both the 800~\degree C-sintered (empty circle marker) and the 600~\degree C-sintered samples show an improved conductivity after 65-hour Mg-vapor anneal. The annealed 800~\degree C-sintered sample (red circle) shows a $T^{-3/2}$-trend as predicted by acoustic-phonon scattering. The negligible change of Seebeck coefficient and thermal conductivity is consistent with our previous study assuming a grain boundary scattering\cite{Kuo2018}. (d)-(g) Corresponding EBSD maps of the samples. Grain growth observed in the annealed samples suggests the improved conductivity in (a) is due to the reduction of grain-boundary density.}
{\tiny {\tiny }}	\label{fig:BiTrans}
\end{figure*}
%\FloatBarrier

\section{Experimental Result}
\subsection{Transport Properties}
%%Introduce the affects of annealing on the carrier concentration/fermi level
A significant enhancement of electrical conductivity at low temperatures was discovered in the \ce{Mg_{3.01}Sb_{1.5}Bi_{0.5}Te_{0.01}} samples after long-term Mg-vapor anneal (Fig.~\ref{fig:BiTrans}(a)). Pellets with different initial grain sizes were prepared via different sintering conditions (\textit{i.e.} at 600~\degree C for 1 hour and at 800~\degree C for 20 minutes). In each condition, a pair of pellets were pressed at the same time in the same instrument, with one as the control sample and the other for annealing at 600~\degree C for 65 hours. For both conditions, annealed samples show higher electrical conductivities compared to the un-annealed control ones. 

In particular, the conductivity of the 800~\degree C-sintered sample follows a $T^{-3/2}$-trend, which indicates an acoustic-phonon scattering mechanism dominating even at room temperature. The unchanged Seebeck (Fig.~\ref{fig:BiTrans}(b)) suggests that the Fermi level of the sample is unaltered, and therefore the improvement of conductivity should be attributed to the reduction of detrimental grain boundary scattering. 

\subsection{Grain Size Measurement}
Electronically, the region near a grain boundary can be more resistive than the bulk due to the disorder and off-stoichiometry at the boundary\cite{Taylor1952,Pike1979}. Decreasing the density of grain boundaries, as well as reducing their resistance are two potential explanations for the improvement of low temperature electrical conductivity shown in Fig.~\ref{fig:BiTrans}(a). Experimentally, the average grain sizes before and after anneal can be measured via electron backscatter diffraction (EBSD). 

Measurable grain growth in the annealed samples can be seen through the comparison of EBSD maps (Fig.~\ref{fig:BiTrans}(d)-(h)). The grain sizes of the un-annealed samples (\textit{i.e.} $<1\ \mu m$ for the 600~\degree C-sintered, and $\approx10\ \mu m$ for the 800~\degree C sintered) is consistent with the values reported in the literature\cite{Kanno2018}, whereas the annealed samples have grain size significantly larger than the un-annealed counterparts (\textit{i.e.} $\approx5\ \mu m$ for the 600~\degree C-sintered, and $>25\ \mu m$ for the 800~\degree C sintered). 


\section{Discussion}
\subsection{Implication of charge transport}
The unchanged Seebeck coefficient after the  grain growth that enhances electrical conductivity supports the conclusion that grain boundary scattering is significant in \ce{Mg3Sb2}-based materials \cite{Kuo2018}. Because the measured Seebeck coefficient is dominated by the bulk electrical properties (\textit{e.g.} density-of-states and Fermi level)\cite{May2017}, when the transport mechanism of the bulk grains changes, the Seebeck coefficient should also change correspondingly. Based on this argument, other hypotheses for why the electrical conductivity increses with temperature in some n-type \ce{Mg3Sb2}, such as point-defect scattering or ionized-impurity scattering\cite{Shuai2017,Mao2017}, would predict a change of bulk properties and therefore a change of Seebeck coefficient. In addition, these hypotheses does not explain the enhanced performance in samples with large grain sizes\cite{Kanno2018}. We therefore conclude that reduction of low-temperature $zT$ must be due to the highly-resistive grain boundaries.

\begin{table}[bth]
	\caption{\label{table:melting} Eutectic and pseudo-eutectic melting temperatures between Mg \& \ce{Mg3Sb2}\cite{Nayeb-Hashemi1984,grube1906alloys,abel1930ternare,grube1934elektrische}, Mg \& \ce{Mg3Sb_{1.5}Bi_{0.5}}, and Mg \& \ce{Mg3Bi2}\cite{Nayeb-Hashemi1985,grube1906alloys,abel1930ternare}. The pseudo-eutectic initial melting temperature  of the alloyed compound was measured through differential thermal analysis in this work (see section.~\ref{eutectic}).}
	\begin{ruledtabular}
		\begin{tabular}{rccc}
			{\ce{Mg3(Sb_{1-x}Bi_{x})_{2}}}&x=0&0.25&1\\
			%\cline{3-4}
			\hline
			$T\mathrm{_{Eut} (\degree C)}$&$629 \pm 2$&$602 \pm 5$&$552 \pm 2$\\
		\end{tabular}
	\end{ruledtabular}
\end{table}

\subsection{Hypothesis of the Grain-growth Mechanism}
%%Paragraph on Grain growth
The grain growth witnessed in annealed samples could be associated with a pseudo-eutectic liquid that forms, which dramatically increases atomic diffusion during annealing.\cite{german2014sintering}  This liquid is called pseudo-eutectic (like in the Al - \ce{Mg2Si} pseudo-binary \cite{FanZ2001EpA2,LiChong2009Mfih}) instead of eutectic because it does not meet the strict criteria of being a single point in ternary space where four phases are in equilibrium at a single temperature.\cite{campbell2012phase, Callister2007} Furthermore, the term pseudo-eutectic is used to differentiate the liquid that we witness from a congruently melted liquid formed entirely from the \ce{Mg_{3}Sb_{1.5}Bi_{0.5}} alloy, which would only occur at a much higher temperature. 

The pseudo- eutectic temperature of initial melting changes with Sb/Bi ratio as shown in Table ~\ref{table:melting} and is found approximately at 90 at.\% Mg. The $\mathrm{Mg_{3.01} Sb_{1.5}Bi_{.5}}$ samples presented here are annealed with extra Mg at a temperature where there is likely to be some liquid phase, which would increase grain growth.\cite{german2014sintering}   A quick non-equilibrium re-solidification of this melt should lead to some compositional segregation of the Sb/Bi ratios in solid-solution, which is called coring. \cite{campbell2012phase, Callister2007}


\section{Conclusions}
In conclusion, we demonstrate an unconventional method for annealing n-type \ce{Mg3Sb2}-based compounds in Mg vapor which preserves their n-type properties during grain growth. We observed that long-term annealing results in a significant enhancement in the electron mobility, which is attributed to a reduction in grain boundary scattering as the grain size increased. Adding this annealing processing step to {Sb1.5Bi0.5} material results in a $zT$ = 0.8 at 300~K allowing it to compete with state-of-the-art n-type thermoelectric materials. 


\footnotesize

\section{Experimental Methods}
\subsection{Sample preparation}
We sealed magnesium turnings (99.98 \%, Alfa Aesar), antimony shots (99.9999 \%, 5N Plus), and Te shot (99.999 \%, 5N Plus) into stainless-steel vials according to the nominal composition in an argon-filled glove box. Elements were mechanically alloyed by high energy-ball milling with a high-energy mill (SPEX 8000D) for two hours. The processed powder was loaded into a graphite die with  half inch diameter and pressed by an induction heating rapid hot press \cite{lalonde2011rapid} for 60 minutes at 873~K or 20 minutes at 1073~K and 45 MPa under argon gas flow.

\subsection{Annealing process}
Hot-pressed pellets were placed into a magnesium oxide crucible (25~mm diameter, 25~mm height). Magnesium turnings (99.98 \%, Alfa Aesar) are added such that both sides of the pellet are in contact with Mg turnings. The crucible was loaded into a graphite die and covered by a piece of graphite foil and graphite spacer to create a quasi-isolating environment for the pellet. The graphite die was heated up by a induction heater to 873~K under argon gas flow. 

\subsection{Measurement of crystal grain sizes}
Electron backscattering diffraction (EBSD) maps were obtained using a scanning electron microscope (Quanta650FEG) equipped with a detector (Oxford Instruments Nordlys).

\subsection{Differential thermal analysis}
Differential Thermal Analysis (DTA) was carried out in a Netzch Jupiter F3. A balled milled samples of \ce{Mg_{.75}(Sb_{.75}Bi_{.25})_.25} was sealed in a carbon coated quartz vials and ramped to 750 \degree C at a rate of 10 \degree C/min. The sample was then cooled at a rate of 10 \degree to 100 \degree C and cycled again to 725 \degree C. 

\subsection{Measurements of transport properties}
Electrical and thermal transport properties were measured from 323 to 573~K. The electrical resistivity and Hall coefficient measurements were determined using the 4-point probe Van der Pauw technique with a 0.8~T magnetic field under high vacuum \cite{borup2015measuring}. The Seebeck coefficients of the samples were obtained using chromel-Nb thermocouples by applying a temperature gradient across the sample to oscillate between $\pm$5~K \cite{iwanaga2011high}. Thermal conductivity was calculated from the relation $\kappa = DdC_p$, where $D$ is the thermal diffusivity measured with a Netzsch LFA 457 laser flash apparatus, $d$ is the geometrical density of the material and $C_p$ is the heat capacity at constant pressure. $C_p$ the compounds were calculated via the polynomial equation proposed by Agne \textit{et al.}\cite{Agne2018}.
For the purpose of comparison, we measured the electrical transport properties (\textit{i.e.} conductivity and Seebeck coefficient) on an ULVAC ZEM-3, as shown in Fig.~\ref{fig:ZEM_data}.ZEM data is used for to compair zT's and weighted mobilities of our samples and literature (Fig:.~\ref{fig:zT}).

\section*{Author contributions}
MW and JJK equally contributed to this project. MW designed the annealing setup. MW and JJK conceived the project and wrote the manuscript.  MW, JJK, and KI synthesized and processed the compounds, performed characterizations, and measured/analyzed transport data. GJS supervised the project. All authors edited the manuscript.

\section*{Acknowledgements}
This research was carried out under a contract with the National Aeronautics and Space Administration and was supported by the NASA Science Missions Directorate's Radioisotope Power Systems Technology Advancement Program. The EBSD in this work made use of the EPIC facility of Northwestern University’s NUANCE Center, which has received support from: the Soft and Hybrid Nanotechnology Experimental (SHyNE) Resource (NSF ECCS-1542205); the MRSEC program (NSF DMR-1720139) at the Materials Research Center; the International Institute for Nanotechnology (IIN); the Keck Foundation; the State of Illinois, through the IIN. DTA and XRD in this work made use of the IMSERC at Northwestern University, which has received support from the Soft and Hybrid Nanotechnology Experimental (SHyNE) Resource (NSF ECCS-1542205); the State of Illinois and International Institute for Nanotechnology (IIN).
\normalsize

\bibliography{Mg3Sb2_annealing.bib}

\end{document}
%
% ****** End of file aipsamp.tex ******