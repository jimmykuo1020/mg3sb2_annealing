%***** Start of file aipsamp.tex ******
%
%   This file is part of the AIP files in the AIP distribution for REVTeX 4.
%   Version 4.1 of REVTeX, October 2009
%
%   Copyright (c) 2009 American Institute of Physics.
%
%   See the AIP README file for restrictions and more information.
%
% TeX'ing this file requires that you have AMS-LaTeX 2.0 installed
% as well as the rest of the prerequisites for REVTeX 4.1
%
% It also requires running BibTeX. The commands are as follows:
%
%  1)  latex  aipsamp
%  2)  bibtex aipsamp
%  3)  latex  aipsamp
%  4)  latex  aipsamp
%
% Use this file as a source of example code for your aip document.
% Use the file aiptemplate.tex as a template for your document.
\documentclass[%
aip,
jmp,
amsmath,amssymb,
preprint,%
%reprint,
]{revtex4-1}

\usepackage{graphicx}% Include figure files
\usepackage{subfig}
\usepackage{dcolumn}% Align table columns on decimal point
\usepackage{bm}% bold math
\usepackage[version=3]{mhchem}%chemical symbols
%\usepackage[mathlines]{lineno}% Enable numbering of text and display math
%\linenumbers\relax % Commence numbering lines
\usepackage[none]{hyphenat} %suppress hyphenation
\usepackage{multirow}
\usepackage{float} %use [H] to force figure placement
\usepackage{color}
\usepackage{placeins}
\usepackage{gensymb}
\usepackage{setspace}
%\setlength{\tabcolsep}{20pt}
\renewcommand{\arraystretch}{1.5}
\usepackage{xr}
\externaldocument{Mg2Sb2_annealing}
\renewcommand{\thefigure}{S\arabic{figure}} %put subsection number on fig. numbering
\renewcommand{\thesection}{S\arabic{section}} %put subsection number on fig. numbering


\begin{document}
\title[]{Supporting Information: Improvement of low-temperature $zT$ in \ce{Mg3Sb2}-based compounds via Mg-vapor annealing}% Force line breaks with \\

\author{Max Wood}
\affiliation{Northwestern University, Evanston, IL 60208, USA}

\author{Jimmy Jiahong Kuo}
\affiliation{Northwestern University, Evanston, IL 60208, USA}

\author{Kazuki Imasato}
\affiliation{Northwestern University, Evanston, IL 60208, USA}

\author{G. Jeffrey Snyder}
\email{jeff.snyder@northwestern.edu}
\affiliation{Northwestern University, Evanston, IL 60208, USA}

\date{\today}

\maketitle
\onecolumngrid


\section{Effect of short-term anneal on the n-type properties}\label{short-term}
Fig.~\ref{fig:SeebeckChangeType} shows a the effect of short-term Mg-vapor anneal on the Seebeck coefficient. The Te-doped \ce{Mg3Sb2} sample was made slightly Mg-poor in the nominal composition and therefore was p-type initially. The sample was annealed in Mg vapor for 1 hour and became n-type afterward. The result demonstrates that Mg-vapor anneal provides an environment that suppresses the formation of Mg vacancies and favors the n-type properties of \ce{Mg3Sb2}-based compounds.


\begin{figure}[bth]
	\includegraphics[width=0.45\textwidth]{./Figures/FIG_SeebeckChangeType_ver02.pdf} 
	\caption{Transition of Te-doped \ce{Mg3Sb2} from the p-type, non-degenerate (blue curve) to n-type, degenerate (red curve) semiconductor after Mg-vapor anneal.}
	\label{fig:SeebeckChangeType}
\end{figure}

\section{Test of sample homogeneity after annealing}
\begin{figure}[bth]
	\includegraphics[width=0.45\textwidth]{./Figures/FIG_PolishTest_muH_ver02.pdf} 
	\caption{Hall mobility of \ce{Mg3Sb_{1.5}Bi_{0.5}} before and after 24-hour anneal. Repeatable n-type Hall mobility can be measured after polishing 0.5~mm and 1.0~mm (\textit{i.e.} originally 1.7~mm), suggesting that the change is not a surface effect.}
	\label{fig:SurfaceEffectTest}
\end{figure}
All the transport data are interpreted with the assumption that samples are homogeneous without surface effects after the anneal. This is verified via repeated measurements after polishing the outer surfaces. Fig.\ref{fig:SurfaceEffectTest} shows that Hall mobility is unaltered after thinning the annealed pellet by 1.0~mm (more than half of the original pellet). 

\section{Pseudo-eutectic temperature measurement}\label{eutectic}
The pseudo-eutectic onset of melting temperature between Mg \& \ce{Mg3Sb_{1.5}Bi_{0.5}} was measured through differential thermal analysis (DTA). Fig.\ref{fig:DTA_data} shows the onset of melting in ball milled powder with composition \ce{Mg_{0.75}(Sb_{0.75}Bi_{0.25})_{0.25}}. 

\begin{figure}[bth]
	\includegraphics[width=0.5\textwidth]{./Figures/FIG_Heating_DTA.pdf} 
	\caption{Differential Thermal Analysis data for ball milled powder of \ce{Mg_{0.75}(Sb_{0.75}Bi_{0.25})_{0.25}} during heating. }
	\label{fig:DTA_data}
\end{figure}

\section{Electrical transport properties measured from ZEM}\label{ZEM3}
For the purpose of comparing $zT$ of similar compounds from the literature, we measure the electrical transport properties (\textit{i.e.} conductivity and Seebeck coefficient) ) on an ULVAC ZEM3, as shown in Fig.~\ref{fig:ZEM_data}.
\begin{figure}[bth]
	\includegraphics[width=0.5\textwidth]{./Figures/FIG_Conductivity&Seebeck_ZEM_ver02.pdf} 
	\caption{Electrical conductivity (cyan, left axis) and Seebeck coefficient (purple, right axis) of the annealed 800C-sintered sample. The conductivity from ZEM-3 (cyan triangles) shows similar trend as our measured through Van der Pauw measurement (cyan circles), whereas the Seebeck coefficient from ZEM-3 (purple triangles) is larger than the measurement setup designed by Iwanaga et al.\cite{Iwanaga2011} at high temperature may be due to cold-finger effect\cite{Borup2015}.}
	\label{fig:ZEM_data}
\end{figure}

\section{Hall Mobility}\label{HallMobility}
Fig.~\ref{fig:muH} shows experimental measurement of Hall mobility. The annealed 800\degree~C-sintered sample has the highest reported Hall mobility ($\approx 170 \mathrm{cm^2/Vs}$) at 300~K among all n-type \ce{Mg_{3}Sb_{1.5}Bi_{0.5}} compounds (see Table.~\ref{table:muHComparison}).
\begin{figure}[bth]
	\includegraphics[width=0.45\textwidth]{./Figures/FIG_HallMobility_ourData.pdf} 
	\caption{Hall mobility of the annealed (red color) and control (blue color) \ce{Mg_{3.01}Sb_{1.5}Bi_{0.5}Te_{0.01}} Both the 800~\degree C-sintered (empty circle marker) and the 600~\degree C-sintered samples show an improved conductivity after 65-hour Mg-vapor anneal.}
	\label{fig:muH}
\end{figure}

\begin{table}[bth]
	\caption{\label{table:muHComparison} Hall mobility at 300~K of the annealed 800\degree~C-sintered sample and data from the literature.}
	\begin{ruledtabular}
		\begin{tabular}{rcccc}
			Ref.&Kanno\cite{Kanno2018}&Zhang\cite{Zhang2017}&Ohno\cite{Ohno2018}\&Shuai\cite{Shuai2017}&This work\\
			%\cline{3-4}
			\hline
			$\mu_H\mathrm{(cm^2/Vs)}$&109&45&$\approx$20&\color{red}170\color{black}\\
		\end{tabular}
	\end{ruledtabular}
\end{table}

\section{X-Ray Diffraction Measurement}
 

\begin{figure}[bth]
	\includegraphics[width=0.45\textwidth]{./Figures/FIG_XRAY.pdf} 
	\caption{Cu $\mathrm{K\alpha}$ x-ray diffraction in reflection geometry of pelletized  samples sythesized for this study. All samples had a composition of  \ce{Mg_{3.01}Sb_{1.49}Bi_{0.5}Te_{0.01}}, but underwent different processing steps. From XRD there appears to be no visible impurity phases.}
	\label{fig:Xray}
\end{figure}


\bibliography{Mg3Sb2_annealing.bib}

\end{document}
%
% ****** End of file aipsamp.tex ******